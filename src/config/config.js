export const SHARED_CHAT_KEY = 'SHARED_CHAT_KEY';
export const DEFAULT_SERVER_PORT = '8090';
export const REGISTRATION_MESSAGE = "Choose a username that would be unique to you." +
    "This name will be publicly seen by all members of the chat. Choose wisely (:";
export const CHAT_NAME_SPACE = "/chat";
export const DEFAULT_SERVER = "localhost";
export const STICKER_DIRECTORY = ":" + DEFAULT_SERVER_PORT + "/static/stickers/";


export const REGISTER_ACTION = "REGISTER";
export const SEND_SHARED_ACTION = "SEND_SHARED";


export const MESSAGE_PACKET = "MESSAGE_PACKET";
export const CONTROL_PACKET = "CONTROL_PACKET";


export const MESSAGE_TEXT = "MESSAGE_TEXT";
export const MESSAGE_STICKER = "MESSAGE_STICKER";



