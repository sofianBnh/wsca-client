import {action, computed, decorate, observable} from "mobx";
import {SHARED_CHAT_KEY} from "../config/config";

// Source https://hackernoon.com/how-to-build-your-first-app-with-mobx-and-react-aea54fbb3265

class ChatStore {

    currentChatIndex = 0;

    chats = [
        {
            key: SHARED_CHAT_KEY,
            name: "Shared Chat",
            icon: "team",
            messages: []
        },
    ];

    addMessage(message) {
        this.chats[0].messages.push(message);
    }

    get currentChatKey() {
        return this.chats.find(chat => {
            return chat.key === this.chats[this.currentChatIndex];
        })
    }

    reset() {

        this.currentChatIndex = 0;

        this.chats = [
            {
                key: SHARED_CHAT_KEY,
                name: "Shared Chat",
                icon: "team",
                messages: []
            },
        ];
    }

}

decorate(ChatStore, {
    currentChat: observable,
    chats: observable,
    sio: observable,

    addMessage: action,
    reset: action,
    currentChatKey: computed,
});

export default ChatStore;