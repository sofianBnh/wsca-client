import {action, decorate, observable} from "mobx";
import io from "socket.io-client";
import {CHAT_NAME_SPACE, DEFAULT_SERVER_PORT} from "../config/config";

class CommonStore {

    sio = null;
    server = "";
    username = "";
    registered = false;
    loading = false;
    error = "";


    saveServer(server) {
        this.server = server;
    }

    saveUsername(username) {
        this.registered = true;
        this.username = username;
    }

    updateError(message) {
        this.error = message;
    }

    buildSIO() {
        const url = this.server + ":" + DEFAULT_SERVER_PORT + CHAT_NAME_SPACE;

        this.sio = io.connect(url, {
            reconnection: false,
            autoConnect: false,
        });

        this.sio.open();

    }

    updateLoading(loading) {
        this.loading = loading;
    }

    reset() {
        this.sio = null;
        this.server = "";
        this.username = "";
        this.registered = false;
        this.loading = false;
        this.error = "";
    }
}


decorate(CommonStore, {

    sio: observable,
    error: observable,
    server: observable,
    loading: observable,
    username: observable,
    registered: observable,


    saveUsername: action,
    saveServer: action,
    updateLoading: action,
    updateError: action,
    buildSIO: action,
    reset: action,

});


export default CommonStore;