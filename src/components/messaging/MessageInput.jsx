import React, {Component} from 'react';
import {Button, Input, Popover} from "antd";
import PropTypes from "prop-types";
import ChatStore from "../../store/Chatstore";
import StickerDrawer from "./StickerDrawer";
import {MESSAGE_STICKER, MESSAGE_TEXT} from "../../config/config";
import CommonStore from "../../store/CommonStore";

class MessageInput extends Component {

    state = {
        message: "",
        visible: false
    };

    changeMessage(e) {
        this.setState({message: e.target.value})
    }

    sendPacket() {
        if (this.state.message) {
            this.props.sendPacket({
                content: this.state.message,
                format: MESSAGE_TEXT,
            });
            this.setState({message: ''})
        }
    }

    sendSticker = (sticker) => {
        if (sticker) {
            this.props.sendPacket({
                content: sticker,
                format: MESSAGE_STICKER
            })
        }
        this.setState({visible: false})
    };

    handleKey(e) {
        if (e.keyCode === 13)
            this.sendPacket();
    }

    handleVisibleChange = (visible) => {
        this.setState({visible});
    };

    render() {

        return (
            <div style={{margin: "0 auto"}}>

                <Input size={"large"} placeholder="Type your message here"
                       style={{display: "inline", width: '80%', borderRadius: '1em 0em 0em 1em'}}
                       value={this.state.message}
                       onChange={e => this.changeMessage(e)}
                       onKeyDown={(e) => this.handleKey(e)}
                />

                <Popover content={<StickerDrawer send={this.sendSticker} commonStore={this.props.commonStore}/>}
                         onVisibleChange={this.handleVisibleChange}
                         visible={this.state.visible}
                         placement={"topLeft"}
                         trigger="click">

                    <Button size={"large"}
                            style={{display: "inline", width: '8%', borderRadius: "0"}}
                            type="ghost" icon="smile"
                            theme="outlined"
                    />
                </Popover>

                <Button size={"large"}
                        style={{display: "inline", width: '12%', borderRadius: '0 1em 1em 0em'}}
                        type="primary" icon="caret-right"
                        theme="outlined" onClick={() => this.sendPacket()}
                />

            </div>
        );
    }
}

MessageInput.propTypes = {
    chatStore: PropTypes.instanceOf(ChatStore),
    commonStore: PropTypes.instanceOf(CommonStore),
    sendPacket: PropTypes.func.isRequired,
};

export default MessageInput;
