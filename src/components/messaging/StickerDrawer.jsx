import React from 'react';
import {STICKER_DIRECTORY} from "../../config/config";
import {STICKERS_NAMES} from "../../store/stickerdb";
import PropTypes from "prop-types";
import CommonStore from "../../store/CommonStore";


function StickerDrawer({send, commonStore}) {

    return (
        <div style={{overflowY: "auto", width: "13em", maxHeight: "50vh", paddingRight: "1em"}}>
            {STICKERS_NAMES.map((sticker, index) => {
                const url = "http://" + commonStore.server + STICKER_DIRECTORY + sticker;

                return <div key={index}
                            className={"sticker"}
                            style={{
                                background: "url(" + url + ") center",
                                backgroundSize: "10em 10em",
                                backgroundRepeat: "no-repeat",
                                resize: "both"
                            }}
                            onClick={() => send(sticker)}
                />
            })}
        </div>
    );
}

StickerDrawer.propTypes = {
    send: PropTypes.func.isRequired,
    commonStore: PropTypes.instanceOf(CommonStore).isRequired,
};

export default StickerDrawer;

