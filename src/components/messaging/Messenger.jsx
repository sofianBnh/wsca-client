import React, {Component} from 'react';
import MessageInput from "./MessageInput";
import {Button, Layout} from "antd";
import Message from "./Message";
import {observer} from "mobx-react";
import PropTypes from "prop-types";
import ChatStore from "../../store/Chatstore";
import CommonStore from "../../store/CommonStore";

const {Footer, Content} = Layout;

class Messenger extends Component {

    scrollToBottom = () => {
        this.messageEnd.scrollIntoView({behavior: "smooth"});
    };

    componentDidMount() {
        this.scrollToBottom();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    render() {

        const {chats, currentChatIndex} = this.props.chatStore;

        return (
            <Layout style={{padding: '0 1em', height: "65vh", backgroundColor: "white",}}>
                <div>

                    <Button type={"danger"} icon={"close"}
                            style={{float: "right",}}
                            shape="circle"
                            onClick={() => this.props.leave()}
                            htmlType={"button"}
                    />

                    <h3 className={"section-header"} style={{flex: "center"}}>{chats[currentChatIndex].name}</h3>

                </div>


                <Content style={{height: "48vh", overflowY: "auto"}}>
                    {chats[currentChatIndex].messages.map((message, index) => {
                        return <Message key={index} {...message} commonStore={this.props.commonStore}/>
                    })}

                    <div style={{float: "left", clear: "both",}} ref={(el) => {
                        this.messageEnd = el
                    }}/>
                </Content>

                <Footer style={{padding: '1em 0', backgroundColor: "white"}}>
                    <MessageInput chatStore={this.props.chatStore} sendPacket={this.props.sendPacket}
                                  commonStore={this.props.commonStore}/>
                </Footer>
            </Layout>
        );
    }
}

Messenger.propTypes = {
    chatStore: PropTypes.instanceOf(ChatStore).isRequired,
    commonStore: PropTypes.instanceOf(CommonStore).isRequired,
    sendPacket: PropTypes.func.isRequired,
    leave: PropTypes.func.isRequired,
};

export default observer(Messenger);
