import React from 'react';
import PropTypes from "prop-types";
import {MESSAGE_STICKER, STICKER_DIRECTORY} from "../../config/config";
import CommonStore from "../../store/CommonStore";

const commonStyle = {
    margin: "4px 1.2em 4px 0",
    padding: "1em 1.3em 0.1em 1.3em",
    maxWidth: "65%",
    minWidth: "7em",
    display: "inline-block",
    boxShadow: "0 1px 3px 0 rgba(0,0,0,.14)"

};

const externalMessageStyle = {
    ...commonStyle,
    color: "white",
    borderRadius: "0.2em 1em 1em 1em",
};

const internalMessageStyle = {
    ...commonStyle,
    float: "right",
    borderRadius: "1em 0.2em 1em 1em",
};

const backgrounds = {
    "IT": {
        background: "#ebedf0",
    },
    "ET": {
        background: "#1890ff",
    },
    "S": {
        background: "white",
    }
};

function Message({commonStore, message, sender, internal = false}) {

    let style = (internal) ? internalMessageStyle : externalMessageStyle;
    return (
        <div style={{overflowY: "auto"}}>

            {(message.format === MESSAGE_STICKER) ?
                <StickerMessage message={message} sender={sender} internal={internal} style={style}
                                server={commonStore.server}/> :
                <TextMessage message={message} sender={sender} internal={internal} style={style}/>
            }
        </div>
    );
}

function StickerMessage({server, message, style, internal, sender}) {
    const url = "http://" + server + STICKER_DIRECTORY + message.content;
    return (
        <div style={{...style, ...backgrounds["S"]}}>
            {(!internal && sender) &&
            <p style={{fontWeight: "bold", fontSize: "1.1em", marginBottom: "0.6em", color: "#666"}}>{sender}</p>}

            <div style={{
                width: "8em",
                height: "8em",
                position: "center",
                margin: "1em",
                transition: ".3s ease",
                background: "url(" + url + ") center",
                backgroundSize: "8em 8em",
                backgroundRepeat: "no-repeat",
                resize: "both"
            }}/>
        </div>
    );
}


function TextMessage({style, sender, internal, message}) {
    return (
        <div style={{...style, ...backgrounds[((internal) ? "I" : "E") + "T"]}}>
            {(!internal && sender) &&
            <p style={{fontWeight: "bold", fontSize: "1.1em", marginBottom: "0.6em"}}>{sender}</p>}

            <p className={"dont-break-out"}> {message.content} </p>
        </div>
    );
}


Message.propTypes = {
    commonStore: PropTypes.instanceOf(CommonStore).isRequired,
    message: PropTypes.object.isRequired,
    sender: PropTypes.string,
};

export default Message;
