import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Alert, Button, Input, Layout} from "antd";
import {observer} from "mobx-react";
import {DEFAULT_SERVER, REGISTRATION_MESSAGE} from "../../config/config";
import CommonStore from "../../store/CommonStore";


const inputStyle = {
    marginBottom: "1em",
    borderRadius: "1em",
    padding: "0 0.5em",
    textAlign: "center"
};

const {Content,} = Layout;

class RegistrationPane extends Component {

    state = {
        server: DEFAULT_SERVER,
        username: "",
    };

    register() {
        if (this.state.username) {
            const {store} = this.props;
            store.updateError("");
            store.updateLoading(true);
            store.saveServer(this.state.server);
            store.buildSIO();
            this.props.startListener();
            this.props.startRegistration(this.state.username);
        }
    }


    handleInputChange(e) {
        this.setState({[e.target.name]: e.target.value})
    }


    handleKey(e) {
        if (e.keyCode === 13)
            this.register();
    }


    render() {

        const {error, loading} = this.props.store;

        return (

            <Layout className={"pane"} style={{width: "38vw", margin: '6vh auto', padding: "3.5em", minWidth: "40em"}}>

                <h1 style={{textAlign: "center", marginBottom: "3em"}}>Registration</h1>

                <Content style={{width: "70%", margin: "2em auto"}}>

                    <p style={{textAlign: "center", fontSize: "1.2em"}}>{REGISTRATION_MESSAGE}</p>

                    <Input style={{...inputStyle, marginTop: "2em",}}
                           name={"server"}
                           size={"large"}
                           placeholder={"Server IP"}
                           value={this.state.server}
                           onChange={e => this.handleInputChange(e)}/>

                    <Input style={inputStyle}
                           name={"username"}
                           size={"large"}
                           placeholder={"Username Here..."}
                           value={this.state.username}
                           onKeyDown={(e) => this.handleKey(e)}
                           onChange={e => this.handleInputChange(e)}/>


                    <Button style={{borderRadius: "1em"}} block type={"primary"} size={"large"}
                            loading={loading}
                            onClick={() => this.register()}
                            htmlType={"button"}>
                        Register
                    </Button>

                    {error &&
                    <Alert type={"error"}
                           style={{marginTop: "1em", borderRadius: "1em", textAlign: "center"}}
                           message={error}
                           closable showIcon/>}

                </Content>

            </Layout>
        );
    }
}

RegistrationPane.propTypes = {
    store: PropTypes.instanceOf(CommonStore).isRequired,
    startListener: PropTypes.func.isRequired,
    startRegistration: PropTypes.func.isRequired,
};

export default observer(RegistrationPane);
