import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Layout} from "antd";
import ChatMenu from "../ChatMenu";
import Messenger from "../messaging/Messenger";
import ChatStore from "../../store/Chatstore";
import CommonStore from "../../store/CommonStore";

const {Content, Sider,} = Layout;

class MessagePane extends Component {
    render() {
        const {chatStore, commonStore} = this.props;
        return (
            <Layout className={"pane"}>

                <Sider style={{background: 'white', width: "40vw", height: "65vh", minWidth: "80em"}}>
                    <ChatMenu store={chatStore} username={this.props.username}/>
                </Sider>

                <Content style={{padding: '0 2em', height: "65vh",}}>
                    <Messenger commonStore={commonStore} chatStore={chatStore} sendPacket={this.props.sendPacket}
                               leave={this.props.leave}/>
                </Content>
            </Layout>
        );
    }
}

MessagePane.propTypes = {
    chatStore: PropTypes.instanceOf(ChatStore).isRequired,
    commonStore: PropTypes.instanceOf(CommonStore).isRequired,
    sendPacket: PropTypes.func.isRequired,
    username: PropTypes.string.isRequired,
    leave: PropTypes.func.isRequired,
};

export default MessagePane;
