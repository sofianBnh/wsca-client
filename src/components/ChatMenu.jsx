import React from 'react';
import {Icon, Menu} from "antd";
import {SHARED_CHAT_KEY} from "../config/config";
import {observer} from "mobx-react";
import PropTypes from "prop-types";
import ChatStore from "../store/Chatstore";

function ChatMenu(props) {


    const {chats} = props.store;
    const {username} = props;

    return (
        <div>

            <h3 className={"section-header dont-break-out"}>{username}</h3>

            <Menu
                mode="inline"
                defaultSelectedKeys={[SHARED_CHAT_KEY]}
                style={{height: '100%'}}
            >

                {chats.map((chat, index) => {
                    return <Menu.Item key={chat.key}>
                        <Icon type={chat.icon}/>{chat.name}
                    </Menu.Item>
                })}

            </Menu>
        </div>
    );
}

ChatMenu.propTypes = {
    store: PropTypes.instanceOf(ChatStore).isRequired,
    username: PropTypes.string.isRequired,
};


export default observer(ChatMenu);