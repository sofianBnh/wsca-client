import {CONTROL_PACKET, MESSAGE_PACKET} from "../config/config";

export default function buildPacket(type, action, {content, format, receiver = null}) {

    switch (type) {
        case MESSAGE_PACKET:
            return {
                type: type,
                action: action,
                receiver: receiver,
                message: {
                    format: format,
                    content: content,
                },
            };

        case CONTROL_PACKET:
            return {
                type: type,
                action: action,
                options: content
            };

        default:
            return null;
    }

}

