import React, {Component} from 'react';
import './App.css';
import {configure} from "mobx"
import ChatStore from "./store/Chatstore";
import CommonStore from "./store/CommonStore";
import RegistrationPane from "./components/panes/RegistrationPane";
import MessagePane from "./components/panes/MessagePane";
import {observer} from "mobx-react";
import {notification} from "antd";
import buildPacket from "./utils/parser";
import {CONTROL_PACKET, MESSAGE_PACKET, REGISTER_ACTION, SEND_SHARED_ACTION} from "./config/config";


configure({enforceActions: "observed"});

const commonStore = new CommonStore();
const chatStore = new ChatStore();

class App extends Component {


    static openNotificationWithIcon(type, title) {
        notification[type]({
            message: title,
        });
    };

    static disconnectFromServer() {
        const {sio} = commonStore;

        if (sio) {
            sio.disconnect()
        }
    }

    static startListener() {

        const {sio} = commonStore;

        if (sio) {

            sio.on('connect', () => {
                App.startErrorListener();
                App.startPacketListener();
            });

            sio.on("connect_error", () => {
                commonStore.updateError("Could not connect to the Server");
                commonStore.updateLoading(false);
            });

            sio.on('disconnect', () => {
                commonStore.reset();
                chatStore.reset();
                App.openNotificationWithIcon("info", "You are now disconnected");
            })

        }

    }

    static startRegistration(username) {

        const {sio} = commonStore;

        if (sio) {
            const registrationPacket = buildPacket(
                CONTROL_PACKET,
                REGISTER_ACTION,
                {content: username}
            );

            sio.emit('control', registrationPacket, () => {
            });

            sio.on('register_error', error => {
                const json = JSON.parse(error);
                commonStore.updateError(json.options);
                commonStore.updateLoading(false);
            });

            sio.on("accept", () => {
                commonStore.updateLoading(false);
                commonStore.saveUsername(username)
            })

        }

    }


    static startErrorListener() {

        const {sio} = commonStore;

        if (sio) {
            sio.on("reject", error => {
                const json = JSON.parse(error);
                App.openNotificationWithIcon("error", json.options);
                commonStore.updateLoading(false);
            })
        }
    }

    static startPacketListener() {

        const {sio} = commonStore;

        if (sio) {
            sio.on('packet', message => {
                const json = JSON.parse(message);
                chatStore.addMessage(json)
            });


            sio.on('sent', (received) => {
                const json = JSON.parse(received);
                chatStore.addMessage({...json, internal: true});
            })
        }
    }

    static sendPacket(data) {
        const {sio} = commonStore;

        const packet = buildPacket(MESSAGE_PACKET, SEND_SHARED_ACTION, data);

        if (sio) {
            sio.emit("packet", packet, () => {
            });
        }
    }

    static route() {
        const {username} = commonStore;

        if (!commonStore.registered) {
            return <RegistrationPane
                store={commonStore}
                startListener={App.startListener}
                startRegistration={App.startRegistration}/>
        } else {
            return <MessagePane
                username={username}
                chatStore={chatStore}
                commonStore={commonStore}
                sendPacket={App.sendPacket}
                leave={App.disconnectFromServer}
            />
        }
    }

    render() {
        return (
            <div className="App">
                <div className={"container"}>
                    {App.route()}
                </div>
            </div>
        );
    }
}

export default observer(App);

