# Web Socket Chat Application: Client



## Overview

This repository is part of an educational project for creating a chat application based on web sockets. The application follows a server-client architecture with a Python server and a React JS client. The user can connect anonymously to the chat application and then start getting messages from the other users. 

The front end will handle two main activities. The first is getting the information from the client for the registration and trying to connect them to the server. The second is allowing the user to send messages and stickers to the chat. Like the server, the client is also event-driven and hence has the same overall architecture of handlers and event triggers. The difference is that it also uses an observer pattern to propagate the changes to the interface. This pattern works by having a store which is a database shared across the application and where components would *observe* it. This means that they would try to stay in sync with it and keep the latest version of the data inside that database. This means that when an event is received, it triggers a handler which would update something in the store which would then update the view. 

For the implementation, we used static functions at the entry point of the application; in this case *App.js*. An example of this is the starting of the listeners:

```javascript
    static startListener() {
        // Getting the socket io instance
        const {sio} = commonStore;

        if (sio) {
            
            // Trigger for the event "connect", 
            // when a connection is established with the server
            sio.on('connect', () => {
                // When it gets triggered
                App.startErrorListener(); // Start the error listner 
                App.startPacketListener(); // Start the packet listner
            });

            ...
        }

    }
```
These functions are then called by components or triggered by an event. When they are triggered they access the stores and modify whatever data they need to (adding message, changing server ip,...) And the application handles the changes accordingly.

## Project Structure

The project is structured as follows:

```
├── package.json # Dependencies of the application
├── public # public resources
│   ├── index.html # Index of the application
│   └── manifest.json # SPA configuration
├── src
│   ├── App.js # Entry Point of the Frontend
│   ├── components # React components
│   ├── config # Configuration files
│   ├── index.js # Index of the front end
│   ├── store # Stores 
│   └── utils # Utilities, Parsers, Validators...
└── yarn.lock # Yarn module manager lock
```

## Dependencies 

To accomplish this projetc we decided to use the following open source libraries:

- [Socketio.js](https://socket.io/)
- [ReactJS](https://reactjs.org/)
- [MobX](https://mobx.js.org/)
- [Sockeio-python](https://python-socketio.readthedocs.io/en/latest/)
- [Click](https://click.palletsprojects.com/en/7.x/)
- [Aiottp](https://aiohttp.readthedocs.io/en/stable/)
- [Eventlet](http://eventlet.net/)
- [Python-deamon](https://travis-ci.org/serverdensity/python-daemon)

## Run Instruction

For starting the Front in development mode the following steps are followed:

Installing node:
```bash
sudo apt-get install curl python-software-properties
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install nodejs
```

Installing yarn:
```bash
sudo npm install -g yarn
```

Downloading the dependencies:

```bash
cd wsca-client/
yarn
```

In case of a problem do the following:

```bash
rm -rf node_modules
sudo apt remove --purge nodejs
sudo apt install nodejs
yarn
```

Starting the front end:

```bash
yarn start
```

Building:

```bash
yarn build
```